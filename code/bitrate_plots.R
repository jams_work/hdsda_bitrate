## Title: Comparing spectrograms and frequency spectrums of different bitrates
## Desc:  Comparing the specs of the datasets
##        Mutliple parts to this file:
##        Part 1 is comparing bitrate differences for method section
##        Part 2 is the spectrograms and freq sepectrum example in intro
##        Part 3 is a line plot of the catsim statistic
## Comment: For repeatability, run transcode_gen.R before this

library(tidyverse)
library(seewave)
library(patchwork)
library(lemon)

source("conv_funs.R")

## some distinct colours https://sashamaps.net/docs/resources/20-colors/
sasha_cols <- c("#e6194b", "#3cb44b", "#4363d8", "#f58231", "#911eb4")

## Part 1: Comparing bitrates of dataset========================================

## read in all necessary audio samples
## first is 128kBps -> 329kBps transcodes
files_128_320 <- list.files("../data/working_sets/medmus_128_320_trans",
  full.names = T
)

## combine with mulaw wavs converted files and normal files
## of different bitrates
files_in <- c(
  list.files("../data/working_sets/medmus_brs", full.names = T),
  list.files("../data/working_sets/medmus_mp3_mulaws", full.names = T),
  files_128_320
)

## create mean frequency spectrums of all files
## this can take a while
res_list <- lapply(files_in, function(x) {
  meanspec(tuneR::readMP3(x),
    plot = F,
    dB = "max0"
  )
})

## parse output, add metadata from file names
## also assign names to sources
res_tb_list <- list()

for (i in seq_len(length(files_in))) {
  res_tb_list[[i]] <-
    res_list[[i]] %>%
    as_tibble() %>%
    add_column(
      source = dirname(files_in[i]),
      file_name = basename(files_in[i])
    )
}

res_tb <- do.call(rbind, res_tb_list) %>%
  mutate(source = case_when(
    str_detect(source, "medmus_brs") ~ "norm",
    str_detect(source, "128_320") ~ "trans_norm",
    str_detect(source, "mulaw") ~ "trans_mulaw"
  ))

## save this table since processing beforehand was so long
write_csv(res_tb, "../data/saved_tables/freq_specs_for_plots.csv")


## seperate out data into trancoded samples for one plot
## and normal data for another
## each requires some custom processing to get the labels right
spec_tb_norm <-
  res_tb %>%
  filter(!is.na(x)) %>%
  filter(source == "norm") %>%
  mutate(
    file_name = str_extract(file_name, "x(.*?)k"),
    bitrate = paste0(str_extract(file_name, "[0-9]+"), "kBps MP3")
  ) %>%
  mutate(bitrate = fct_rev(as_factor(bitrate)))

spec_tb_trans <-
  res_tb %>%
  filter(!is.na(x)) %>%
  filter(source != "norm") %>%
  mutate(source = factor(source,
    levels = c("trans_norm", "trans_mulaw"),
    labels = c(
      "128kbps to 320kbps",
      "\u03bc-law"
    )
  ))

## plot 2d-histograms
spec_plot <-
  spec_tb_norm %>%
  ggplot(aes(x = x, y = y)) +
  stat_bin2d(bins = 100) +
  facet_wrap(bitrate ~ .) +
  scale_fill_viridis_c(trans = "log10") +
  scale_x_continuous(breaks = seq(from = 0, to = 22, by = 4)) +
  theme_classic(base_size = 10) +
  theme(
    panel.grid.major = element_line(colour = "grey90"),
    legend.background = element_blank(),
    plot.margin = margin(t = 0, r = 0, b = -0.6, l = 0, unit = "cm")
  ) +
  labs(
    y = "Signal strength - Decibels (dB)",
    x = "",
    fill = bquote(atop("Count", (log[10]))),
    title = "A: MP3 bitrates"
  )

spec_plot_trans <-
  spec_tb_trans %>%
  ggplot(aes(x = x, y = y)) +
  stat_bin2d(bins = 100) +
  facet_wrap(source ~ .,
    labeller = labeller(label_parsed),
    ncol = 1
  ) +
  scale_fill_viridis_c(trans = "log10") +
  scale_x_continuous(breaks = seq(from = 0, to = 22, by = 4)) +
  theme_classic(base_size = 10) +
  theme(
    panel.grid.major = element_line(colour = "grey90"),
    legend.background = element_blank(),
    legend.position = "none",
    plot.margin = margin(t = 0, r = 0, b = -0.6, l = 0, unit = "cm")
  ) +
  labs(
    y = "",
    x = "",
    fill = bquote(atop("Count", (log[10]))),
    title = "B: 128kBps transcodes"
  )

## create a axis label only plot so the x-axis can be shared
x_lab <- ggplot(
  tibble(
    x = 1,
    y = 1,
    x_label = "Signal frequency - Kilohertz (kHz)"
  ),
  aes(x, y, label = x_label)
) +
  geom_text(size = 3.5) +
  coord_cartesian(clip = "off") +
  theme_void() +
  theme(
    plot.margin = margin(t = -0.8, r = 0, b = -0.3, l = 0, unit = "cm")
  )

## now combine all the elements
plot_combo <-
  (spec_plot + spec_plot_trans) +
  plot_layout(
    guides = "collect",
    widths = c(2, 1)
  )

# with fixed x axis
plot_combo_x_fix <-
  (plot_combo / x_lab) +
  plot_layout(
    heights = c(35, 1)
  )

ggsave(plot_combo_x_fix,
  filename = "../diagrams/freq_specs.png",
  dpi = 600,
  units = "cm",
  width = 17.9387,
  height = 9
)

## Part 2: spectrograms and frequency spectrums example=========================
## for intro section

## select audio samples
file_sel <- "medley_8"
files_ls <- list.files("../data/working_sets/medmus_samps_brs",
  full.names = T
)

## just get medley_8
files_detected <- str_detect(files_ls, file_sel)
mp3_sel <- files_ls[files_detected]

mp3_read <- sapply(mp3_sel, readMP3)

names(mp3_read) <- basename(names(mp3_read))

## get for other file qualities as well
orig_wavs <- list.files("../data/working_sets/medmus",
  full.names = T
)

mulaws <- list.files("../data/working_sets/medmus_mp3_mulaws",
  full.names = T
)

wavs_to_read <- c(
  mulaws[str_detect(mulaws, file_sel)],
  orig_wavs[str_detect(orig_wavs, file_sel)]
)

## read in wav files
wavs_read <-
  sapply(
    wavs_to_read,
    function(x) {
      sampleWav2(
        file_x = x,
        write_file = F
      )
    }
  )

names(wavs_read) <- basename(names(wavs_read))

## combine the vectors of samples, remove 192 and 256
samps_read <-
  c(
    wavs_read[2],
    mp3_read[4],
    mp3_read[1],
    wavs_read[1]
  )

## change the names here for the plotting later
new_names <- c(
  "Original WAV",
  "320kBps MP3",
  "128kBps MP3",
  "\u03bc-law transcode"
)

names(samps_read) <- new_names

## generate frequency spectrums
f_specs_ls <- list()
for (i in seq_len(length(samps_read))) {
  foo_fspec <- meanspec(samps_read[[i]],
    plot = F,
    dB = "max0"
  )

  foo_tb <- as_tibble(foo_fspec)
  foo_tb$file <- names(samps_read)[i]

  f_specs_ls[[i]] <- foo_tb
}

tb_fspecs <- do.call(rbind, f_specs_ls)

## plot the frequency spectrums on one graph
fspecs_plot <-
  tb_fspecs %>%
  mutate(file = factor(file, levels = new_names)) %>%
  ggplot(aes(x = x, y = y, colour = file)) +
  geom_line(alpha = 0.7) +
  theme_classic(base_size = 10) +
  theme(
    panel.grid.major = element_line(colour = "grey90"),
    legend.background = element_blank()
  ) +
  labs(
    y = "Signal strength - Decibels (dB)",
    x = "Signal frequency - Kilohertz (kHz)",
    colour = "Sample"
  ) +
  scale_x_continuous(breaks = seq(from = 0, to = 22, by = 4)) +
  scale_colour_manual(values = sasha_cols)

fspecs_plot <- reposition_legend(fspecs_plot,
  position = "bottom left",
  plot = F
)

ggsave(
  plot = fspecs_plot,
  file = "../diagrams/fspecs_plot.png",
  dpi = 600,
  units = "cm",
  width = 8.89,
  height = 7
)

## loop through the files, creating spectrograms
## cant use lapply simply here because of accessing names
samps_spectro_ls <- list()
for (i in seq_len(length(samps_read))) {
  foo_spectro <- ggspectro(samps_read[[i]], ovlp = 50)
  foo_data <- tibble(foo_spectro$data)
  foo_data$file <- names(samps_read)[i]

  samps_spectro_ls[[i]] <- foo_data
}

## compile spectrogram data into a tibble
tb_spectro <-
  do.call(rbind, samps_spectro_ls) %>%
  filter(!is.infinite(amplitude)) %>%
  mutate(file = factor(file, levels = new_names))

## plot
tb_spectro %>%
  ggplot(aes(x = time, y = frequency, fill = amplitude)) +
  geom_raster() +
  facet_wrap(file ~ ., ncol = 4) +
  scale_fill_viridis_c(limits = c(-110, 0), na.value = "white") +
  theme_classic(base_size = 10) +
  theme(legend.position = "bottom") +
  labs(
    x = "Time - Seconds",
    y = "Audio frequency - Kilohertz (kHz)",
    fill = "Signal amplitude - Decibels (dB)"
  ) +
  scale_y_continuous(breaks = seq(from = 0, to = 22, by = 4)) +
  ggsave(
    file = "../diagrams/file_spectros.png",
    dpi = 600,
    units = "cm",
    width = 17.9387,
    height = 8.89
  )

## Part 3: Measurements plot====================================================
## plot the catsim statistic using all the training data. within sample bitrate
## on the x axis.

## read in the training data, generated earlier
tb_meas_in <- read_csv("../data/saved_tables/medmus_feats_16k.csv")

## process the data to remove redundant stuff, normalise to 320kBps,
## and isolate catsim (just need one)
tb_meas_proc <-
  tb_meas_in %>%
  mutate(
    file = paste(source, id, sep = "_"),
    file_br = factor(file_br,
      levels = c(320, 256, 192, 128)
    )
  ) %>%
  filter(
    ghost == "ng",
    metric %in% c("b.catsim")
  ) %>%
  mutate(metric = str_to_title(str_remove(metric, "a.|b."))) %>%
  select(-ghost, -source, -id) %>%
  group_by(file, metric, file_br) %>%
  mutate(val_320 = value - value[v_br == 320])

## plot
meas_plot <-
  tb_meas_proc %>%
  ggplot(aes(x = v_br, y = value, colour = file_br)) +
  geom_smooth(method = "loess") +
  #  facet_wrap(metric ~ ., scales = "free_y") +
  theme_classic(base_size = 10) +
  theme(
    panel.grid.major = element_line(colour = "grey90"),
    legend.position = "top",
    legend.spacing.x = unit(0.075, "cm")
  ) +
  scale_colour_manual(values = sasha_cols) +
  scale_x_reverse(breaks = c(128, 192, 256, 320)) +
  labs(
    y = "Catsim similarity metric", x = "Sample rencoding bitrate (kBps)",
    colour = "Original sample\nbitrate (kBps)"
  )

ggsave(meas_plot,
  filename = "../diagrams/meas_plot.png",
  dpi = 600,
  units = "cm",
  width = 8.89,
  height = 8.89
)
