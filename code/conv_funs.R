## Title:  Funs for conversions
## Author: x19175329@student.ncirl.ie
## Desc:   Functions for file conversion and handling

## ghostLister: creates a new list, subtract spectrograms from each other
## this fun assumes the highest bitrate comes first
## this has been changed! Now the first one will be the 320, not nothing!
## actually, changed it back to nothing. Made more sense.
ghostLister <-
  function(list_x) {
    list_out <- list()

    for (i in 1:length(list_x)) {
      if (i == 1) {
        list_out[[1]] <- list_x[[1]] - list_x[[1]]
      } else {
        list_out[[i]] <- list_x[[1]] - list_x[[i]]
      }
    }

    names(list_out) <- names(list_x)

    list_out
  }


## function to convert one file to a series of MP3s
ffmpegConv <-
  function(file_x,
           conv_set_x = c("320k", "256k", "192k", "128k", "64k"),
           out_folder = "temp_swap") {
    require(snakecase, quietly = T)

    ## create temp dir for files
    if (dir.exists(out_folder) == F) {
      dir.create(out_folder)
    }

    file_name <- basename(tools::file_path_sans_ext(file_x))

    # save location of this
    swap_loc <- paste(out_folder, sep = "/")

    for (i in 1:length(conv_set_x)) {

      # specifies output path
      # also cleans out spaces
      out_file <- paste0(
        out_folder,
        "/x",
        conv_set_x[i],
        "_",
        gsub(" ", "\\ ", file_name, fixed = T),
        ".mp3"
      )

      # create the ffmpeg command
      # also sub out any spaces in the file path
      sys_cmd <-
        paste0(
          "ffmpeg -i ",
          paste(gsub(" ", "\\ ", file_x, fixed = T)),
          " -b:a ",
          conv_set_x[i],
          " ",
          "-loglevel quiet ",
          out_file
        )

      system(sys_cmd)
    }

    paste(out_folder, list.files(path = out_folder), sep = "/")
  }


## samples the center of a wav file, outputs it and returns its location
## a version which uses inbuilt functions for sampling
## requires ffmpeg
sampleWav2 <- function(file_x,
                       out_path = "wav_samps",
                       samp_secs = 6,
                       write_file = T) {
  require(tuneR, quietly = T)

  ## create temp dir for files
  if (dir.exists(out_path) == F) {
    dir.create(out_path)
  }

  ## get file duration with ffmpeg
  file_length <- system(
    paste(
      "ffprobe -v error -show_entries format=duration \ -of default=noprint_wrappers=1:nokey=1 ",
      gsub(" ", "\\ ", file_x, fixed = T) # catching spaces
    ),
    intern = T
  )

  file_length <- as.numeric(file_length)

  ## find midpoint of file for sampling
  midpoint <- round(file_length / 2, 0)

  samp_range <- round(
    c(
      midpoint - samp_secs / 2,
      midpoint + samp_secs / 2
    ),
    0
  )

  file_ext <- tools::file_ext(file_x)

  ## get file extension and switch between wav and mp3
  ## file needs to be wav. If it isnt, convert to wav
  if (file_ext == "mp3") {
    foo_mp3 <- tuneR::readMP3(file_x)
    tuneR::writeWave(foo_mp3, "temp_wav.wav")

    foo_wav <- tuneR::readWave("temp_wav.wav",
      from = samp_range[1],
      to = samp_range[2],
      units = "seconds"
    )
  } else {
    (

      foo_wav <- tuneR::readWave(file_x,
        from = samp_range[1],
        to = samp_range[2],
        units = "seconds"
      )
    )
  }

  file_x_base <- basename(file_x)

  if (file_ext == "mp3") {
    file.remove("temp_wav.wav")
  }

  ## if write file is true, output a file, otherwise keep in env

  if (write_file == T) {

    ## control if the file doesnt have extension written at start
    out_file_path <- paste(out_path,
      tools::file_path_sans_ext(file_x_base),
      sep = "/"
    )
    out_file_path <- paste0(out_file_path, ".wav")

    tuneR::writeWave(
      foo_wav,
      out_file_path
    )

    out_file_path
  } else {
    foo_wav
  }
}

## sampleDir: applies sampling to entire folder. Returns the paths
## if rand is an integer, it samples that number from the folder
sampleDir <- function(dir_x,
                      out_path = "wav_samps",
                      sample_seconds = 5,
                      rand = NA) {
  dir_files <- list.files(dir_x,
    full.names = T
  )

  # sample and set the rest to NAs
  if (!is.na(rand)) {
    foo_samps <- sort(sample(1:length(dir_files), rand))

    for (i_ind in 1:length(dir_files)) {
      if (!i_ind %in% foo_samps) {
        dir_files[i_ind] <- NA
      }
    }
  }

  out_vector <- c()

  print("sampleDir progress...")
  tpb <- txtProgressBar(1, length(dir_files), style = 3)

  # loop to perform the sampling
  for (i in 1:length(dir_files)) {
    if (!is.na(dir_files[i])) {
      out_vector[i] <- sampleWav2(
        dir_files[i],
        out_path,
        sample_seconds
      )
    }
    setTxtProgressBar(tpb, i)
  }

  out_vector
}


## function to read in folder of mp3s and save as list
## sorts them by size, to get 320 at top
fold2List <- function(folder_x) {
  list_foo <- list()
  files_foo <- list.files(folder_x, full.names = T)

  files_foo_sorted <- names(sort(sapply(files_foo, file.size),
    decreasing = T
  ))

  for (i in 1:length(files_foo_sorted)) {
    list_foo[[i]] <- readMP3(files_foo_sorted[i])
    names(list_foo)[i] <- basename(files_foo_sorted[i])
  }
  list_foo
}

## function to change the file names of a folder
## a combination of dir names and indices
## it outputs a vector of the file names
renameFiles <- function(folder_x) {
  file_names <- list.files(folder_x,
    full.names = T
  )

  file_renames <- paste(
    folder_x,
    "/",
    basename(folder_x),
    "_",
    as.character(1:length(file_names)),
    sep = ""
  )

  for (i in 1:length(file_names)) {
    file.rename(
      from = file_names[i],
      to = file_renames[i]
    )
  }
  file_renames
}

## a function to take wavs from one folder and randomly populate
## another folder with converted files
randBitratesGen <- function(
                            folder_in,
                            folder_out,
                            n_rand = 100,
                            bitrates = c("320k", "256k", "192k", "128k", "64k")) {

  ## create temp dir for files
  if (dir.exists(folder_out) == F) {
    dir.create(folder_out)
  }

  files_foo <- list.files(folder_in)
  rand_files <- sample(files_foo, n_rand)

  print("randBitratesGen progress...")
  pb <- txtProgressBar(1, length(rand_files), style = 3)

  for (ii in 1:length(rand_files)) {
    ffmpegConv(paste0(folder_in, rand_files[ii]),
      sample(bitrates, 1),
      out_folder = folder_out
    )
    setTxtProgressBar(pb, ii)
  }
  list.files(folder_out, full.names = T)
}


## this function copies all the files in a folder
## and makes different bitrate versions of them
## wraps aroung ffmpegConv
allBitratesGen <- function(
                           folder_in,
                           folder_out,
                           bitrates = c("320k", "256k", "192k", "128k", "64k")) {

  ## create temp dir for files
  if (dir.exists(folder_out) == F) {
    dir.create(folder_out)
  }

  files_foo <- list.files(folder_in)
  files_foo_l <- length(files_foo)

  print("allBitratesGen progress...")
  pb <- txtProgressBar(1, files_foo_l, style = 3)

  for (ii in 1:files_foo_l) {
    ffmpegConv(paste0(folder_in, files_foo[ii]),
      bitrates,
      out_folder = folder_out
    )
    setTxtProgressBar(pb, ii)
  }
  list.files(folder_out, full.names = T)
}


## convert sound file to matrix, using hanning and SFT
## used for theSims
conv2Mat <- function(tune_x, cutoff_fq = 0) {
  require(seewave, quietly = T)

  foo_spec <- spectroCustom(tune_x, cutoff_fq = cutoff_fq)

  foo_mat <- as.matrix(foo_spec$amp,
    nrow = length(foo_spec$freq),
    ncol = length(foo_spec$time)
  )

  foo_mat[is.infinite(foo_mat)] <- NA

  foo_mat
}


## a wrapper around seewave::spec which can remove lower freqs
specCustom <- function(sample_x, cutoff_fq = 0) {
  spec_x <- seewave::spec(sample_x, plot = F)

  if (cutoff_fq > 0) {

    # need to make sure there is actually stuff to cut-off
    if (max(spec_x[, 1], na.rm = T) > cutoff_fq) {
      higher_freqs <- which(spec_x[, 1] > cutoff_fq)
      spec_x <- spec_x[higher_freqs, ]
    }
  }

  spec_x <- spec_x[complete.cases(spec_x), ]
}

## a wrapper around seewave::spectro which can remove lower freqs
spectroCustom <- function(sample_x, cutoff_fq = 0) {
  spectro_x <- seewave::spectro(sample_x, plot = F)

  if (cutoff_fq > 0) {
    low_freqs <- which(spectro_x$freq < cutoff_fq)
    spectro_x$amp <- spectro_x$amp[-low_freqs, ]
    spectro_x$freq <- spectro_x$freq[-low_freqs]
  }

  spectro_x
}


## cleans up the messy output.

listClean <- function(list_x, extra = T) {

  # first this is to extract metadata from file name
  # left with a list of tibbles
  tidy_list <- list()
  for (i in 1:length(list_x)) {
    foo_messy <- list_x[[i]]
    foo_strings <- str_split(names(foo_messy), "_")
    sample_name <- foo_strings[[1]][3]
    file_br <- str_extract(foo_strings[[1]][2], "[0-9]+")
    v_br <- str_extract(sapply(foo_strings, function(x) x[1]), "[0-9]+")

    names(foo_messy) <- rev(as.character(v_br))

    # make into a tibble and add columns
    foo_tidyr <-
      foo_messy %>%
      rownames_to_column("metric") %>%
      as_tibble() %>%
      unnest(cols = everything()) %>%
      pivot_longer(
        names_to = "v_br",
        cols = 2:(length(v_br) + 1)
      ) %>%
      add_column(
        id = sample_name,
        file_br = file_br
      )

    tidy_list[[i]] <- foo_tidyr
  }

  # some extra processes to make it wide format
  if (extra == T) {
    ## makes a wide tib
    tb <- do.call(rbind, tidy_list) %>%
      mutate(feat = paste(metric, v_br, sep = "_")) %>%
      select(id, file_br, feat, value) %>%
      pivot_wider(
        names_from = "feat",
        values_from = "value"
      )
  } else {
    tb <- do.call(rbind, tidy_list)
  }
  tb
}
