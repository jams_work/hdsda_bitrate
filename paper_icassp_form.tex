% Template for ICASSP-2021 paper; to be used with:
%          spconf.sty  - ICASSP/ICIP LaTeX style file, and
%          IEEEbib.bst - IEEE bibliography style file.
% --------------------------------------------------------------------------
\documentclass{article}
\usepackage{spconf,amsmath,graphicx}

% Example definitions.
% --------------------
\def\x{{\mathbf x}}
\def\L{{\cal L}}

% Title.
% ------
\title{MP3 COMPRESSION CLASSIFICATION THROUGH AUDIO ANALYSIS STATISTICS}
%
% Single address.
% ---------------
\name{Jamie McFarlane, Bharathi Chakravarthi}
\address{Dept. of Computing, National College of Ireland}
%
\begin{document}
%\ninept
%
\maketitle
%
\begin{abstract}
MP3 audio compression can be undesirable in circumstances where high-quality music presentation is required and there is a lack of automated, evidenced, and open-source methods to determine this. This study introduced a new and accessible approach to discriminate between compression levels. Machine learning classifiers were trained on feature sets of audio analysis statistics, derived from multiple stepwise re-encodings of compressed audio samples. Two classifiers, a stacked model and a XGboost-based model, had comparable accuracies to previous examples in the literature and marketplace (Stacked: 0.947, XGboost: 0.970, Literature reference: 0.965, Commercial reference: 0.980). For transcoded samples, which hide compression levels with post-processing, the new classifiers were less accurate than existing methods. However, all methods were inaccurate in identifying transcodes with a large amount of artificial noise added via the $\mu$-law encoder.
\end{abstract}
%
\begin{keywords}
Audio coding, Audio compression, Data mining, Machine learning, Spectrogram
\end{keywords}
%
\section{Introduction}
\label{sec:intro}

MPEG Layer III (MP3) encoding is a widespread method of audio compression which balances file size and audio quality. A bitrate of 128 kilobits per second (kBps) is considered sufficient for most presentation modes, but there is a demand for audio with higher bitrates, especially in circumstances where presentation on high-end equipment is valued~\cite{brandenburg_mp3_1999, harris2011do, hines_perceived_2014}.

The compression level is indicated in the metadata of the MP3 file. However, this can be disguised and lower bitrates (e.g. 128kBps) may be distributed as more valued higher bitrate files. Transcoding is the simplest way to accomplish this. Here a lower bitrate can be re-encoded to a higher one (e.g. 128kBps to 320kBps), but it retains the prior compression artifacts. Post-processing can also be applied to obfuscate this by reintroducing data into the upper frequencies of an audio sample and confounding detection methods. For instance, $\mu$-law transcoding can be used to introduce noise into the upper regions of the frequency spectrum which has been cleared by MP3 compression. Bandwidth extension methods can also replicate elements of lower frequencies into the upper ranges, though they are generally less accessible methods~\cite{dietz2002spectral}.

An important paradigm in tackling audio compression discrimination has been through mathematical transformations such as the Fast Fourier transformation (FFT)~\cite{kandadai_audio_2008}. Products of this approach are spectrograms and frequency spectrums, that can map time, frequency and signal intensity on to separate axes. These data types are amenable to supervised machine learning. For example, an approach using a Polynomial Support Vectors Machines (PSVM) model has been used to classify MP3 files of varying compression levels when focusing on frequencies above 16kHz~\cite{dalessandro_mp3_2009}. Another approach using Convolutional Neural Networks (CNNs) was also reportedly successful, but required a large and difficult to acquire dataset~\cite{hennequin_codec_2017}.

However, despite previous examples in the literature and on the market, consumers are limited in their ability to automatically verify compression levels. Available methods are either closed-source, unevidenced or are simply unavailable~\cite{dalessandro_mp3_2009, hennequin_codec_2017, decker_fakin_2019, gar_software_similarity_2019}.
In this study, a new approach for determining compression was introduced. Sound and image analysis statistics, such as entropy and image degradation, were derived from audio samples, and compiled to create a dataset which was used to train machine learning classifiers. Different methods of pre-processing were evaluated in a data mining style approach. This included subtracting spectrograms of different bitrates from each other to leave only the compression artifacts for analysis (i.e.~\emph{ghost} audio)~\cite{maguire_ghost_2014}.

\section{METHODOLOGY}
\label{sec:method}

\subsection{Proposed technique}
At a high-level, the approach applied statistical measures to individual audio samples. These indices were formed into a dataset to train classifiers which could discriminate between compression levels. However, there are normalisation problems when analysing audio samples directly as characteristics of the data can vary greatly between them. Additionally, many potentially useful measures rely on comparisons between samples. A novel way to deal with these issues is to make copies of the samples with stepwise changes in compression. The rate of change in the statistical indices can be tracked between these copies and allow for the creation of data to discriminate between MP3 bitrates. If one starts with a sample with little compression, and re-encodes this sample into different compression levels (e.g. 320kBps, 256kBps, 192kBps, 128kBps), the stepwise emergence of compression artifacts would be observed. If the starting sample is of already highly compressed, the rate of this emergence in each step will be smaller. For example, re-encoding a 128kBps file with either 320kBps or 128kBps compression will only have a small effect as large amounts of the data would already have been previously removed. This method generates~\emph{re-encoding curves} of the indices which are different shapes, depending on the original sample bitrate [Fig.~\ref{fig:comp_curves}].

\begin{figure}
  \centering
  \includegraphics{diagrams/meas_plot_small.png}
  \caption{An example of re-encoding curves, using the CatSIM statistic. CatSIM is a 0 - 1 bounded statistical $\beta$ index, which compares the structural similarity between matrices. These results are from applying the feature engineering method on the entire MP3 dataset (\emph{n} = 508). The lines are Locally Estimated Scatterplot Smoothing (LOESS) curves with 95\% confidence intervals. }
  \label{fig:comp_curves}
\end{figure}

This study repeated this method over many MP3 samples to form a dataset. Four copies of an individual MP3 sample were made with different levels of compression (i.e. 320kBps, 256kBps, 192kBps and 128kBps). Statistical indices were calculated from each of these copies and were arranged to form a curve in order of the compression level. The 34 different indices were sourced from functions found in the open-source packages~\emph{seewave}, \emph{SPUTNIK} and \emph{catsim}~\cite{seewave, sputnik, thompson_catsim_2020}. $\alpha$-indices require a single audio sample, and were applied to all of these compressed versions to form a 4-point curve. $\beta$-indices require pairs of samples, and compared each step in compression back to the highest bitrate (e.g. 320kBps vs 256kBps).

In a data-mining approach, different combinations of pre- and post-processing were explored which multiplied the size of the feature set. The first variation modified the input audio sample between normal audio and~\emph{ghost} audio. The latter form subtracted each of the re-encoded copies from the 320kBps version. The second variation was an attempt at feature reduction and took the slopes between the raw statistical indices of the re-encoding curves. These variations produced four different processing possibilities (i.e. 2~$\times$ 2) and in total, 136 different types of curves were used.


\begin{figure*}
  \centering
  \includegraphics{diagrams/freq_specs.png}
  \caption{Combined frequency spectrums of all MP3 samples used in the present study, visualised as 2D-histograms. For each panel, \emph{n} = 127. A: MP3 files at the 128, 192, 256 and 320kBps bitrates. These display frequency cutoffs around 16kHz, 19kHz, 19.5kHz and 20kHz respectively. B: Transcoded 128kBps MP3 samples. The 128kBps - 320kBps samples have the file header data of 320kBps files. The $\mu$-law transcodes have noise added up to 20kHz and the header data of 320kBps files.}
\label{fig:freq_specs}
\end{figure*}


\subsection{Dataset and software requirements}
Music audio WAV files from the MusDB and MedleyDB V.2.0 datasets were used as an uncompressed source (\emph{n} = 127)~\cite{musdb18-hq, medleypaper}\footnote{Data in the MusDB dataset which originated in the DSD100 dataset were manually removed as permission from this source could not be sought.}. To form the MP3 dataset, each WAV file was encoded into 128, 192, 256 and 320kBps versions (\emph{n} = 508). Note that this was distinct from creating the re-encoding curves. Two sets of 127 transcoded MP3 files were also created, originating from the 128kBps files [Fig.~\ref{fig:freq_specs}B]. The 128 to 320kBps transcode set had the header data of 320kBps files. The \(\mu\)-law transcode set encoded the 128kBps files with the $\mu$-law encoder which added noise up to 20kHz, followed by a final conversion to 320kBps MP3. The main software requirements were the R language, for data processing and modelling, and ffmpeg for audio file encoding~\cite{ffmpeg20, r_core_team_r_2020}.


\subsection{Model training and evaluation}
Two different predictive classifiers were considered. The first was a stacked model, with 136 unique submodels for each combination of indice and pre-processing method. The second used the XGboost algorithm which baselined the effect of creating submodels using the individual feature sets. The classifiers were compared with two existing methods. The first was previously described in the literature and recreated for this study on the new data. This was a PSVM classifier, focused on frequencies above 16kHz and reported high accuracy~\cite{dalessandro_mp3_2009}. The second pre-existing method was the commercial product Fakin' The Funk (FTF), which purports to identify~\emph{faked} audio files~\cite{decker_fakin_2019}.

There were two stages of evaluation. The first evaluated discrimination between MP3 bitrates. The second evaluated the identification of the original bitrate of transcoded samples. The scope was limited to evaluate 128kBps to 320kBps, and 128kBps to \(\mu\)-law transcodes. Feature importance was also analysed to inform future work.

To evaluate the stacked and XGboost models, nested five-fold Monte-Carlo cross-validation (MCCV) was performed with 75-25\% splits in the data. The stacked model used radial basis function kernel SVMs (RBFSVM) for all of the sub-models, and used least absolute shrinkage and selection operator (LASSO) regression to blend and prioritise the sub-model predictions. For computational efficiency, an additional feature selection step was performed at the start of each outer fold for the stacked model. This fitted simple RBFSVM models for each submodel and selected the best 50, based on their training accuracy. Since this is an ordinal classification problem, both the stacked and XGboost models performed regressions against the file bitrate and the results rounded to 128, 192, 256 or 320kBps. Following the evaluation stage using nested MCCV, the models were retrained on all available data to create final models. These were used to evaluate classification performance for transcoded samples.
Further compromises were made to save on computational resources. Firstly, only the middle six seconds of each sample were used. Secondly, only the frequencies above 16kHz were used for indices which required FFT transformed data as inputs. This would likely also improve performance, as compression artifacts are most prevalent in this region.

Estimates of the reference PSVM classifier performance were calculated through (unnested) five-fold MCCV, using hyperparameters specified in the original paper~\cite{dalessandro_mp3_2009}. A final model was then trained over the entire dataset. The commercial software, FTF, simply analysed the entire MP3 dataset using its default settings.

\section{RESULTS AND EVALUATION}

\subsection{MP3 compression classification}

For each of the models, the predictions from the MCCV folds were combined. This followed tests of model stability which suggested that there was no inter-rater differences between the models from the MCCV folds, and that they could be considered equivalent (Stacked: \(Q\)_(_4_) = 0.417, \(p\) = 0.981; XGboost: \(Q\)_(_4_) = 0.313, \(p\) = 0.989; PSVM: \(Q\)_(_4_) = 0.212, \(p\) = 0.995)~\cite[p. 387 - 394]{Fleiss2003}. These results were then combined with the FTF results and accuracies were compared.

\begin{figure}
  \centering
  {\includegraphics{diagrams/conf_mat_narrow.png}}
  \caption{Confusion matrices of MP3 bitrate prediction. Stacked, XGboost and PSVM models results were pooled from hold out sets. FTF used the entire MP3 dataset. In each case, \emph{n} = 508. Accuracies are percentages, with \emph{n} in brackets.}
  \label{fig:conf_mat}
\end{figure}

Balanced accuracy was used for evaluation, owing to $n$ varying between the groups where MCCV was used. The stacked and XGboost models returned accuracies of 0.947 and 0.970 respectively. The reference methods, FTF and PSVM, returned 0.983 and 0.965 respectively. Considering the confusion matrices of the methods, both FTF and the PSVM model were negatively biased and tended to overestimate the compression of the samples, while the stacked and XGboost models appeared to be more balanced in their misclassification [Fig.~\ref{fig:conf_mat}]. The stacked model produced the highest percentage of any error type, where 13.92\% of 128kBps samples were misclassifed as 192kBps. The PSVM model made errors with the biggest distance, where two 320kBps samples were misclassified as 128kBps.

The results of the transcoded MP3 files considered accuracy as the test data was only from a single class (\textit{n}=127). For the 128kBps to 320kBps transcodes, the new approach was less effective than the reference methods. FTF and the PSVM model had accuracies of 1.000 and 0.923 respectively, with the latter only giving misclassifications of 192kBps. The stacked model misclassified all samples as 320kBps (95.3\%) and 256kBps (4.7\%), while the XGboost model misclassified 21.3\% samples as 192kBps. For the $\mu$-law encoded transcodes, all methods performed poorly. FTF and XGboost misclassified all samples as 320kBps. The stacked model misclassified nearly all samples as 320kBps (95.28\%), with a few exceptions being classifed as 256kBps (4.72\%). The PSVM model had the best performance, but still only classified 19.69\% correctly as 128kBps. The misclassifications were split between 320kBps (77.95\%) and 256kBps (2.36\%).

\subsection{Feature importance}
Feature importance scores were derived from submodel weighting for the stacked model, and internal gain scores for XGboost. In both cases, these were from the final models trained over the entire dataset. The most important statistics used by the primary and secondary models included CatSIM, cumulative frequency spectra difference and structural similarity [Tab.~\ref{tab:sigs}]. Combining the top five statistics from each model some patterns could be identified. The majority of these were $\beta$-indices (9/10). Different types of data processing methods were also favoured, with the majority being from slope transformed data rather than raw data (7/10).~\emph{Ghost} data as a pre-processing step did not feature in the top five features of either model.

\begin{table}[ht]
\caption{Top five features for stacked and XGboost models. Stacked used submodel weighting, XGboost used gain score.}
\centering
\begin{tabular}{llrrr}
  \hline
  \hline
Model & Indice & Score \\
  \hline
Stacked & CatSIM & 0.24 \\
 & Cumulative freq. spectra distance & 0.20 \\
 & Structural similarity & 0.13 \\
 & Manhattan distance & 0.12 \\
 & Kulback-Leiber distance & 0.12 \\
XGboost & Cumulative freq. spectra distance & 0.45 \\
 & Kulback-Leiber distance & 0.25 \\
 & Log spectral distance & 0.11 \\
 & Total entropy & 0.04 \\
 & Kolmogorov-Smirnov distance & 0.01 \\
   \hline
   \hline
\end{tabular}
\label{tab:sigs}
\end{table}

\section{DISCUSSION}
Overall, the evaluation of this new approach suggests that it can produce models which are similar in performance to existing methods at discriminating between MP3 samples. However, existing methods currently perform better at transcode detection. The results of the stacked model at detecting 128kBps to 320kBps transcodes was surprisingly poor and it could be modeling some unknown aspect of this sample type. When it comes to $\mu$-law transcoding, all methods performed poorly. This is an area that should be universally improved upon. Future investigations should also evaluate the detection of bandwidth extension methods if possible.

In terms of execution speed, the proposed method is currently slower than existing methods. Analysing transcoded samples took the PSVM model ~$\sim$3 seconds per sample, while the XGBoost model too~$\sim$26 seconds and the stacked model took~$\sim$30 seconds. Removing unimportant features, reducing the number of encodings, reducing I/O and re-writing the code in a lower level language would all help significantly.

Whether the proposed approach is built upon or not, future methods could incorporate some of the learnings of this study. A stacked model combining the PSVM model with a different model based on other statistical measures is one possiblility. Another could build on the PSVM approach, that uses the slopes between points as a feature reduction method.

This study was limited by a small sample size and a narrow scope of statistical indices for feature selection. Additionally, the 16kHz cutoff, which was enforced to reduce computation time and improve performance, could be challenged in future work and may work well using samples with naturally little audio data above this threshold. Other popular~\emph{lossy} encoders such as Advanced Audio Coding (AAC) and Opus could be included~\cite{brandenburg_mp3_1999, rfc1654}.

The classifiers, as well as the recreation of the PSVM, have been made available online as a command line interface program at: https://gitlab.com/jammcfar/kbps\_detect\_proto.
\vfill\pagebreak

% References should be produced using the bibtex program from suitable
% BiBTeX files (here: strings, refs, manuals). The IEEEbib.bst bibliography
% style file from IEEE produces unsorted bibliography list.
% -------------------------------------------------------------------------
\bibliographystyle{IEEEbib}
\bibliography{strings,refs}

\end{document}
